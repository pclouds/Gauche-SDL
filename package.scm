(define-gauche-package "gauche-sdl"
  :version "0.7"
  :description "Gauche bindings for libsdl 1.x"
  :require (("Gauche" (>= "0.9.7")))
  :providing-modules (sdl
                      sdl.collide
                      sdl.gfx
                      sdl.image
                      sdl.mixer
                      sdl.net
                      sdl.net.ext
                      sdl.smpeg
                      sdl.ttf)
  :authors ("arihasu <foo.yobina@gmail.com>"
            "Duy Nguyen <pclouds@gmail.com>")
  :licenses ("MIT")
  :repository "https://gitlab.com/pclouds/Gauche-SDL")
